﻿<<Note this assignment is incomplete>>

To run the application enter >> gradle bootRun

Go to localhost:8080/h2-console and click connect.

If the employee table is empty enter the following sql.

insert into employee
values(10001,'Len','Ganley', 'len@stance.com', 'wagga');
insert into employee
values(10002,'Bob','Todd', 'bob@gargoyle.com', 'mamma');


Then go to localhost:8080/user to see all the employees

Go to localhost:8080/user/10001 to see an individual employee

I didn't get around to testing the PUT and DELETE methods 