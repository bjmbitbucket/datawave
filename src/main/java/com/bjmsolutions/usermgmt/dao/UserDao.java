package com.bjmsolutions.usermgmt.dao;

import com.bjmsolutions.usermgmt.model.User;

import java.util.List;

public interface UserDao {


    public List findAllUsers();

    public User findUserById(Long id);

    public boolean deleteUser(Long id);

    public boolean updateUser(Long id, User user);


}
