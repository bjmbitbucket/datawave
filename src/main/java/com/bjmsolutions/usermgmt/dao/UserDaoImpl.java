package com.bjmsolutions.usermgmt.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;

import com.bjmsolutions.usermgmt.dao.UserDao;
import com.bjmsolutions.usermgmt.model.User;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;


@Repository
public class UserDaoImpl implements UserDao {

    @PersistenceContext
    private EntityManager entityManager;


    public List findAllUsers(){
        Criteria criteria = entityManager.unwrap(Session.class).createCriteria(User.class);
        return criteria.list();
    }

    public User findUserById(Long id){
        return entityManager.find(User.class, id);
    }

    public boolean deleteUser(Long id){
        User user = findUserById(id);
        if (user != null) {
            entityManager.remove(user);
            return true;
        }
        return false;
    }

    public boolean updateUser(Long id, User user){
        entityManager.merge(user);
        return true;
    }


}