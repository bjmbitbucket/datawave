package com.bjmsolutions.usermgmt.service;

import java.util.List;
import java.util.Optional;

import com.bjmsolutions.usermgmt.dao.UserDao;
import com.bjmsolutions.usermgmt.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserDao userDao;

    public List findAllUsers() {
        return userDao.findAllUsers();
    }

    public Optional<User> findUserById(Long id) {
        return Optional.ofNullable(userDao.findUserById(id));
    }

    public boolean deleteUser(Long id) {
        return userDao.deleteUser(id);
    }

    public boolean updateUser(Long id, User user) {
        return userDao.updateUser(id, user);
    }

}
