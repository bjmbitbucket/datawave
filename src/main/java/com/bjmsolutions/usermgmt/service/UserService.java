package com.bjmsolutions.usermgmt.service;

import java.util.List;
import java.util.Optional;

import com.bjmsolutions.usermgmt.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

public interface UserService {

    public List findAllUsers();

    public Optional<User> findUserById(Long id);

    public boolean deleteUser(Long id);

    public boolean updateUser(Long id, User user);

}
