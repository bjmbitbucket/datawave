
create table employee
(
   id integer not null,
   firstname varchar(255) not null,
   lastname varchar(255) not null,
   email varchar(255) not null,
   password varchar(255) not null,
   primary key(id)
);